root_dir = "";
r = [];         /* .jigdo file rows     */
s = [];         /* .jigdo sums          */
l = [];         /* .jigdo links         */
fid = 0;

function begin()
{
    var msg = "Thank you for using Jigdo-Ivory, a browser"      +
            " jigdo client.<br><br>Please ensure that the"      +
            " directory that you choose is writable by "        +
            "the current user.  Currently, the client uses"     +
            " hardcoded<br>locations for the .jigdo file, "     +
            "meaning it will only download the etch-i386-C"     +
            "D-1.iso  In the future, it should be possible"     +
            "<br>to point to other .jigdo files and downlo"     +
            "ad any .isos compatible with jigdo formatting."    +
            " Thanks for using Ivory, and <br>please remember"  +
            "that this software is very young, and performance" +
            " is not comparable with a native client at this"   +
            " time.<br><br>Please enter the directory you "     +
            "wish to save the files in:<br>Note: the /tmp "     +
            "directory is chosen by default because it is "     +
            "writable, but should not be a permanent location"  +
            " for your <br>jigdo-ivory files, it is suggested " +
            "to create a directory under /home for your files." +
            "<form name='dir' action='javascript:setdir();'>"   +
            "<input type='text' name='dir' size='32'>"          +
            "<input type='button' value='set' onClick='javas"   +
            "cript:setdir();'></form>";
    document.getElementById('status').innerHTML = msg;
    document.dir.dir.value = "/tmp/jigdo-ivory/";
    msg = "Please ensure that the directory you choose" +
            " is appropriate for your environment";
    document.getElementById('msg').innerHTML = msg;
}

function list()
{
 //   document.getElementById('msg').innerHTML = "Please wait....";
    var mdata = dl("http://bluehelm.net/ivory/debian-40r3-i386-CD-1.jigdo");

    if(!mdata)
        alert("download error!");
    xpwrite(mdata, root_dir + "debian-40r3-i386-CD-1.jigdo");

    i = 0;
    first = 0;  //i, first, last = vars
    last = 0;
    cur_time = new Date();

    while(mdata)
    {
        r[i] = mdata.substring(0, mdata.indexOf("\n"));
        mdata = mdata.substring(mdata.indexOf("\n")+1);

        if(r[i].indexOf("[Parts]") != -1)
            first = i + 1;
        if(r[i].indexOf("[Servers]") != -1)
            last = i - 1;
        i += 1;
    }

    srv = r[last + 2];  //var
    srv = srv.substring(srv.indexOf("=")+1, srv.indexOf("/ ")+1);

    for(i = first; i < last; i++)
    {
        s[i] = r[i].substring(0, 22);
        l[i] = r[i].substring(r[i].indexOf(":")+1);
    }
/*
    for(i = first; i < last; i++)
    {
        document.getElementById('status').innerHTML = "downloading: " + l[i];

        var mdata = dl(srv + l[i]);
        
        xpwrite(mdata, root_dir + l[i].substr(l[i].lastIndexOf("/")+1));
        j.reset();
        j.set(mdata, s[i]);
        var percent = Math.floor((i-first)/(last-first)*100);
        document.getElementById('per').innerHTML = "Roughly " + percent;
        document.getElementById('per').innerHTML += " percent completed";
        jig_md5();
    }
*/
    i = first;
    fid = setTimeout("task()", 1);
}

function setdir()
{
    var dir = document.dir.dir.value;
    var msg = "Please be aware that this utility downloads "    +
              "files via http and saves them locally.  While"   +
              " this is normal behavior for any download "      +
              "manager, typically web browsers do not allow"    +
              " files to be saved locally. This means that "    +
              "your permission will be requested to allow "     +
              "jigdo-ivory to save files. In order for the "    +
              "utility to operate successfully, you must "      +
              "allow this to be done.  For those who are "      +
              "concerned, this is an open source project "      +
              "written in HTML and javascript, feel free to "   +
              "determine if it is appropriate for you to use "  +
              "this utility.";
    alert(msg);    
    netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
    var dirfile = Components.classes["@mozilla.org/file/local;1"]
            .createInstance(Components.interfaces.nsILocalFile);
    dirfile.initWithPath(dir);
    
    msg = null;
    if(dirfile.exists())
    {
        if(!dirfile.isDirectory())
            msg = "This is not a directory, please choose a "   +
                    "path that is not an existing filename.";
        if(!dirfile.isWritable())
            msg = "This directory is not writable!";
    }
    else
        dirfile.create(1, 0777);
    
    if(msg)
        alert(msg);
    else
    {
        root_dir = "" + dir;
        if(root_dir.charAt(root_dir.length-1) != "/")
            root_dir += "/";
        list();
    }
}

function task()
{
    if(fid)
        clearTimeout(fid);
    fid = 0;

    var percent = Math.floor((i-first)/(last-first)*100);
    document.getElementById('per').innerHTML = "Roughly " + percent;
    document.getElementById('per').innerHTML += " percent completed";

    if(j.lock)
        fid = setTimeout("task()", 50);
    else
    {
        document.getElementById('status').innerHTML = "downloading: " + l[i];

        var mdata = dl(srv + l[i]);
        
        xpwrite(mdata, root_dir + l[i].substr(l[i].lastIndexOf("/")+1));
        j.reset();
        j.set(mdata, s[i]);
        i += 1;
        jig_md5();
    }
}
