jig = new function jig() /* jigsum "globals" */
{
    this.b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
    this.chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

                       /* jig.tab is modified to use jigsum -_ instead of +/     */
    this.tab     = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

    this.mask = (1 << this.chrsz) - 1;
    this.speed   = 300;  /* higher is better, but can cause timeout problems */
    this.fid = 0;        /* timeout id holder                                */
    this.errors = 0;
}
/*
 *  This object holds the intermediate variables needed for core_md5().
 *  When calculating large md5sums, timeouts occur.  Periodically calling
 *  the function solves this issue, as a result this object is used.
 */
j = new function Jigsum()
{
    this.set = function(s, t)
    {
        this.str = s;
        this.tgt = t;
        this.lock = true;
    }
    
    this.reset = function()
    {
        this.a =  1732584193;
        this.b = -271733879;
        this.c = -1732584194;
        this.d =  271733878;
        this.i =  0;
        this.t =  0;
        this.x =  Array();
    
        this.len  = 0;
        this.lock = false;
        this.sum  = "";         /* this is the result sum */
        this.str  = "";         /* be sure to set this before calling jig_md5 */
        this.tgt  = "";         /* this is the target sum */
    }
    
    this.reset();
}

/*
 *  This is the function to be called,
 *  checksum is found at j.sum
 */
function jig_md5()
{
    j.len = j.str.length * jig.chrsz;
    jig.fid = setTimeout("str2binl()", 1);
}

/*
 *  This is where the magic happens,  If timeouts occur, lower jig.speed
 */
function core_md5()
{
    if(jig.fid)
        clearTimeout(jig.fid);
    jig.fid = 0;
    count = 0;
    new_time = new Date();
    minutes = Math.floor((new_time - cur_time)/60000);
    seconds = Math.floor((new_time - cur_time)/1000) % 60;
    document.getElementById('msg').innerHTML = 'Time elapsed: ' + minutes +
                ' minutes and ' + seconds + ' seconds.';

    while(j.i < j.x.length && count < jig.speed)
    {
        var olda = j.a;
        var oldb = j.b;
        var oldc = j.c;
        var oldd = j.d;

        j.a = md5_ff(j.a, j.b, j.c, j.d, j.x[j.i+ 0], 7 , -680876936);
        j.d = md5_ff(j.d, j.a, j.b, j.c, j.x[j.i+ 1], 12, -389564586);
        j.c = md5_ff(j.c, j.d, j.a, j.b, j.x[j.i+ 2], 17,  606105819);
        j.b = md5_ff(j.b, j.c, j.d, j.a, j.x[j.i+ 3], 22, -1044525330);
        j.a = md5_ff(j.a, j.b, j.c, j.d, j.x[j.i+ 4], 7 , -176418897);
        j.d = md5_ff(j.d, j.a, j.b, j.c, j.x[j.i+ 5], 12,  1200080426);
        j.c = md5_ff(j.c, j.d, j.a, j.b, j.x[j.i+ 6], 17, -1473231341);
        j.b = md5_ff(j.b, j.c, j.d, j.a, j.x[j.i+ 7], 22, -45705983);
        j.a = md5_ff(j.a, j.b, j.c, j.d, j.x[j.i+ 8], 7 ,  1770035416);
        j.d = md5_ff(j.d, j.a, j.b, j.c, j.x[j.i+ 9], 12, -1958414417);
        j.c = md5_ff(j.c, j.d, j.a, j.b, j.x[j.i+10], 17, -42063);
        j.b = md5_ff(j.b, j.c, j.d, j.a, j.x[j.i+11], 22, -1990404162);
        j.a = md5_ff(j.a, j.b, j.c, j.d, j.x[j.i+12], 7 ,  1804603682);
        j.d = md5_ff(j.d, j.a, j.b, j.c, j.x[j.i+13], 12, -40341101);
        j.c = md5_ff(j.c, j.d, j.a, j.b, j.x[j.i+14], 17, -1502002290);
        j.b = md5_ff(j.b, j.c, j.d, j.a, j.x[j.i+15], 22,  1236535329);
    
        j.a = md5_gg(j.a, j.b, j.c, j.d, j.x[j.i+ 1], 5 , -165796510);
        j.d = md5_gg(j.d, j.a, j.b, j.c, j.x[j.i+ 6], 9 , -1069501632);
        j.c = md5_gg(j.c, j.d, j.a, j.b, j.x[j.i+11], 14,  643717713);
        j.b = md5_gg(j.b, j.c, j.d, j.a, j.x[j.i+ 0], 20, -373897302);
        j.a = md5_gg(j.a, j.b, j.c, j.d, j.x[j.i+ 5], 5 , -701558691);
        j.d = md5_gg(j.d, j.a, j.b, j.c, j.x[j.i+10], 9 ,  38016083);
        j.c = md5_gg(j.c, j.d, j.a, j.b, j.x[j.i+15], 14, -660478335);
        j.b = md5_gg(j.b, j.c, j.d, j.a, j.x[j.i+ 4], 20, -405537848);
        j.a = md5_gg(j.a, j.b, j.c, j.d, j.x[j.i+ 9], 5 ,  568446438);
        j.d = md5_gg(j.d, j.a, j.b, j.c, j.x[j.i+14], 9 , -1019803690);
        j.c = md5_gg(j.c, j.d, j.a, j.b, j.x[j.i+ 3], 14, -187363961);
        j.b = md5_gg(j.b, j.c, j.d, j.a, j.x[j.i+ 8], 20,  1163531501);
        j.a = md5_gg(j.a, j.b, j.c, j.d, j.x[j.i+13], 5 , -1444681467);
        j.d = md5_gg(j.d, j.a, j.b, j.c, j.x[j.i+ 2], 9 , -51403784);
        j.c = md5_gg(j.c, j.d, j.a, j.b, j.x[j.i+ 7], 14,  1735328473);
        j.b = md5_gg(j.b, j.c, j.d, j.a, j.x[j.i+12], 20, -1926607734);
    
        j.a = md5_hh(j.a, j.b, j.c, j.d, j.x[j.i+ 5], 4 , -378558);
        j.d = md5_hh(j.d, j.a, j.b, j.c, j.x[j.i+ 8], 11, -2022574463);
        j.c = md5_hh(j.c, j.d, j.a, j.b, j.x[j.i+11], 16,  1839030562);
        j.b = md5_hh(j.b, j.c, j.d, j.a, j.x[j.i+14], 23, -35309556);
        j.a = md5_hh(j.a, j.b, j.c, j.d, j.x[j.i+ 1], 4 , -1530992060);
        j.d = md5_hh(j.d, j.a, j.b, j.c, j.x[j.i+ 4], 11,  1272893353);
        j.c = md5_hh(j.c, j.d, j.a, j.b, j.x[j.i+ 7], 16, -155497632);
        j.b = md5_hh(j.b, j.c, j.d, j.a, j.x[j.i+10], 23, -1094730640);
        j.a = md5_hh(j.a, j.b, j.c, j.d, j.x[j.i+13], 4 ,  681279174);
        j.d = md5_hh(j.d, j.a, j.b, j.c, j.x[j.i+ 0], 11, -358537222);
        j.c = md5_hh(j.c, j.d, j.a, j.b, j.x[j.i+ 3], 16, -722521979);
        j.b = md5_hh(j.b, j.c, j.d, j.a, j.x[j.i+ 6], 23,  76029189);
        j.a = md5_hh(j.a, j.b, j.c, j.d, j.x[j.i+ 9], 4 , -640364487);
        j.d = md5_hh(j.d, j.a, j.b, j.c, j.x[j.i+12], 11, -421815835);
        j.c = md5_hh(j.c, j.d, j.a, j.b, j.x[j.i+15], 16,  530742520);
        j.b = md5_hh(j.b, j.c, j.d, j.a, j.x[j.i+ 2], 23, -995338651);
    
        j.a = md5_ii(j.a, j.b, j.c, j.d, j.x[j.i+ 0], 6 , -198630844);
        j.d = md5_ii(j.d, j.a, j.b, j.c, j.x[j.i+ 7], 10,  1126891415);
        j.c = md5_ii(j.c, j.d, j.a, j.b, j.x[j.i+14], 15, -1416354905);
        j.b = md5_ii(j.b, j.c, j.d, j.a, j.x[j.i+ 5], 21, -57434055);
        j.a = md5_ii(j.a, j.b, j.c, j.d, j.x[j.i+12], 6 ,  1700485571);
        j.d = md5_ii(j.d, j.a, j.b, j.c, j.x[j.i+ 3], 10, -1894986606);
        j.c = md5_ii(j.c, j.d, j.a, j.b, j.x[j.i+10], 15, -1051523);
        j.b = md5_ii(j.b, j.c, j.d, j.a, j.x[j.i+ 1], 21, -2054922799);
        j.a = md5_ii(j.a, j.b, j.c, j.d, j.x[j.i+ 8], 6 ,  1873313359);
        j.d = md5_ii(j.d, j.a, j.b, j.c, j.x[j.i+15], 10, -30611744);
        j.c = md5_ii(j.c, j.d, j.a, j.b, j.x[j.i+ 6], 15, -1560198380);
        j.b = md5_ii(j.b, j.c, j.d, j.a, j.x[j.i+13], 21,  1309151649);
        j.a = md5_ii(j.a, j.b, j.c, j.d, j.x[j.i+ 4], 6 , -145523070);
        j.d = md5_ii(j.d, j.a, j.b, j.c, j.x[j.i+11], 10, -1120210379);
        j.c = md5_ii(j.c, j.d, j.a, j.b, j.x[j.i+ 2], 15,  718787259);
        j.b = md5_ii(j.b, j.c, j.d, j.a, j.x[j.i+ 9], 21, -343485551);
    
        j.a = safe_add(j.a, olda);
        j.b = safe_add(j.b, oldb);
        j.c = safe_add(j.c, oldc);
        j.d = safe_add(j.d, oldd);
        
        j.i += 16;
        count += 1;
    }

    if(j.i < j.x.length)                    //test for continue looping
        jig.fid = setTimeout("core_md5()", 1);
    else                                    //otherwise, move to next piece
    {
        j.x   = Array(j.a, j.b, j.c, j.d);
        j.str = "";
        j.i   = 0;
        jig.fid = setTimeout("binl2b64()", 1);
    }
}

/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
  return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
  return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
  return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
  return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
  return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert a string to an array of little-endian words
 * If jig.chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl()
{
    if(jig.fid)
        clearTimeout(jig.fid);
    jig.fid = 0;
    count = 0;

    while(j.i < j.len && count < jig.speed * 500)
    {
        j.x[j.i>>5] |= (j.str.charCodeAt(j.i / jig.chrsz) & jig.mask) << (j.i%32);
        j.i   += jig.chrsz;
        count += 1;
    }

    if(j.i < j.len)
        jig.fid = setTimeout("str2binl()", 1);
    else
    {
        j.i = 0;
        /* append padding */
        j.x[j.len >> 5] |= 0x80 << ((j.len) % 32);
        j.x[(((j.len + 64) >>> 9) << 4) + 14] = j.len;
        jig.fid = setTimeout("core_md5()", 1);
    }
}

function binl2b64()
{
    if(jig.fid)
        clearTimeout(jig.fid);
    while(j.i < j.x.length * 4)
    {
        j.t = (((j.x[j.i   >> 2] >> 8 * ( j.i   %4)) & 0xFF) << 16)
                | (((j.x[j.i+1 >> 2] >> 8 * ((j.i+1)%4)) & 0xFF) << 8 )
                |  ((j.x[j.i+2 >> 2] >> 8 * ((j.i+2)%4)) & 0xFF);
        for(var k = 0; k < 4; k++)
        {
            if(j.i * 8 + k * 6 > j.x.length * 32) j.str += jig.b64pad;
            else j.str += jig.tab.charAt((j.t >> 6*(3-k)) & 0x3F);
        }
        j.i += 3;
    }

    j.sum = j.str;

    document.getElementById('sum').innerHTML = j.sum;
    if(j.sum == j.tgt)
        document.getElementById('sum').innerHTML += ' +match:success+';
    else
        jig.errors += 1;
    document.getElementById('sum').innerHTML += '<br>' + jig.errors + ' errors.';
    j.lock = false;
    fid = setTimeout("task()", 1);
}
