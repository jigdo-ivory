function xpwrite(mdata, mfilename)
{
    netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");

    var outfile = Components.classes["@mozilla.org/file/local;1"]
            .createInstance(Components.interfaces.nsILocalFile);
    outfile.initWithPath(mfilename);
    
    var output = Components.classes["@mozilla.org/network/file-output-stream;1"]
            .createInstance(Components.interfaces.nsIFileOutputStream);
    output.init(outfile, -1, -1, null);
    
    var ostr = Components.classes["@mozilla.org/binaryoutputstream;1"]
            .createInstance(Components.interfaces.nsIBinaryOutputStream);
    ostr.setOutputStream(output);

    ostr.writeBytes(mdata, mdata.length);
}